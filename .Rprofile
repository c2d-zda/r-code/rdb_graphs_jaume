# set environment variables
Sys.setenv(RENV_PATHS_LOCKFILE = normalizePath("renv/renv.lock",
                                               winslash = "/",
                                               mustWork = TRUE))
# set R options
options(
  # base R options
  warnPartialMatchArgs = TRUE,
  warnPartialMatchAttr = TRUE,
  warnPartialMatchDollar = TRUE,
  # package options
  gt.html_tag_check = TRUE,
  renv.config.pak.enabled = FALSE, # disabled since it currently fails to handle various renv records
  rmarkdown.html_dependency.header_attr = FALSE
)

# load renv
source("renv/activate.R")

# pkg loading and rlang stuff
## attach magrittr pipe operators
if (nzchar(system.file(package = "magrittr"))) {
  library(magrittr,
          include.only = c("%>%", "%<>%", "%T>%", "%!>%", "%$%"))
}

if (nzchar(system.file(package = "rlang"))) {

  ## register rlang's default global handlers
  rlang::global_handle()

  ## attach rlang operators
  library(rlang,
          include.only = c("%|%", "%||%"))
}

# set RNG seeds
set.seed(42L)

if (nzchar(system.file(package = "htmlwidgets"))) {
  htmlwidgets::setWidgetIdSeed(seed = 42)
}

# restore renv state from lockfile (after RStudio is launched to be able to see progress)
setHook(hookName = "rstudio.sessionInit",
        value = function(newSession) {

          show_progress <- nzchar(system.file(package = "cli")) && interactive()

          if (show_progress) {
            cli_id <- cli::cli_progress_step(msg = "Restoring renv state...",
                                             msg_done = "Restoring renv state done. Ready to go.",
                                             .auto_close = FALSE)
          }

          renv::restore(clean = TRUE,
                        prompt = FALSE)

          if (show_progress) {
            cli::cli_progress_done(id = cli_id)
          }
        },
        action = "append")

# level up R, RStudio and Quarto
if (nzchar(system.file(package = "salim"))) {

  setHook(hookName = "rstudio.sessionInit",
          value = function(newSession) {

            salim::lvl_up_r(path_min_vrsn = "assets/.R_version",
                            update_min_vrsn = isTRUE(Sys.info()[["user"]] == "salim"))
            salim::lvl_up_quarto(path_min_vrsn = "assets/.quarto_version",
                                 update_min_vrsn = isTRUE(Sys.info()[["user"]] == "salim"))
            salim::lvl_up_rstudio(path_min_vrsn = "assets/.RStudio_version",
                                  update_min_vrsn = isTRUE(Sys.info()[["user"]] == "salim"))
          },
          action = "append")
}
