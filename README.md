# National referendums over time by custom region grouping

We received a request from Jaume Lopez Hernandez about a higher resolution graph of

![Referendums around the world since the end of the eighteenth century](screenshot.jpg)

for the Catalan version of Referendums. A quick dive.

In [`rdb_graphs_jaume.qmd`](rdb_graphs_jaume.qmd) we try to build some similar or related graphs. The rendered version of this script is deployed to
[**c2d-zda.gitlab.io/rdb_graphs_jaume/**](https://c2d-zda.gitlab.io/rdb_graphs_jaume/)

## Build

To render `rdb_graphs_jaume.qmd` to HTML and run the static image export, execute[^readme-1]:

``` r
fs::dir_create("output")
quarto::quarto_render(input = "index.qmd",
                      output_format = "html")
unlink(c("output/index.html",
         "output/index_files"),
       recursive = TRUE)
fs::file_move(path = "index.html",
              new_path = "output/")
fs::file_move(path = "index_files",
              new_path = "output/")
```

[^readme-1]: We can't directly render to the output folder due to a current [deficiency in
    `quarto::quarto_render()`](https://github.com/quarto-dev/quarto-r/issues/81).

    An alternative to the above would be to use a [Quarto project](https://quarto.org/docs/projects/quarto-projects.html) and set `output-dir: "output"`, i.e.
    create a `_quarto.yml` à la:

    ``` yml
    project:
      type: default
      output-dir: _output
    ```

    Then we can simply run `quarto::quarto_render(output_format = "html")`.

## Deploy

To deploy the output folder to <https://c2d-zda.gitlab.io/rdb_graphs_jaume/>, run:

```         
yay::deploy_static_site(from_path = "output",
                        to_path = "../../c2d-zda.gitlab.io/public/rdb_graphs_jaume/")
```

The above assumes your local clone of the [`c2d-zda/c2d-zda.gitlab.io`](https://gitlab.com/c2d-zda/c2d-zda.gitlab.io) repository lives at
`../../c2d-zda.gitlab.io`.
